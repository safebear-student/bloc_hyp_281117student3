/**
 * Track the trade of a commodity from on trader to another
 * @param {org.acme.mynetwork.Trade } trade - the trade to be processed
 * @transaction
 */

 function tradeCommodity(trade){
     trade.tradeCommodity.owner = trade.newOwner;
     return getAssetRegestriy('ord.acme.mynetwork.Commodity')
        .then(function(assetRegistry){
            return assetRegistry.update(trade.commodity);
        });
 }